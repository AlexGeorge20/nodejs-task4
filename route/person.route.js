const {Router}= require('express');
const { personListController, personCheckIdControler, personDeleteIdControler, personCreateController, personUpdateController } = require('../controller/person.controller');
const router=Router();
let path="/person"


//LIST
router.get(`${path}`,personListController)
//CHECK ID
router.get(`${path}/:id`,personCheckIdControler)
//Delete ID
router.delete(`${path}/:id`,personDeleteIdControler)
//CREATE
router.post(`${path}`,personCreateController)

//UPDATE
router.put(`${path}/:id`,personUpdateController)

module.exports= {personRouter : router}