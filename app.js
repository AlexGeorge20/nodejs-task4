const express=require('express');
const { personRouter } = require('./route/person.route');
const app= express();

app.use(express.json())
app.use(personRouter)
module.exports = {app:app};