const Logger = require("nodemon/lib/utils/log");
const { personListService, personCheckIdService, personDeleteIdService, personCreateService, personUpdateService } = require("../service/person.service");

const personListController= async(req,res)=>{
    const temp =await personListService()
    console.log("READ",temp);
    res.status(201).send(temp[0])
}

const personCheckIdControler=async(req,res)=>{
    let id= req.params.id;
    const response= await personCheckIdService(id);
    res.send(response[0])
}
const personDeleteIdControler=async(req,res)=>{  
    const id= req.params.id;
    const response=await personDeleteIdService(id);
    console.log("DELreSPOnse",response[0]);
    res.send(response[0])
  
}
const personCreateController=async(req,res)=>{  
    const newName= req.body.newName;
    // console.log("REQname",newName);
    const response=await personCreateService(newName);
    console.log("reSPOnse",response[0]);
    res.send(response[0])
  
}
const personUpdateController=async(req,res)=>{
       const newName=req.body.newName;
    //    console.log('bodyname',newName);
       const id=req.params.id;
       const response=await personUpdateService(id,newName)
       console.log("UpdateRESponse",response[0]);
       res.send(response[0])
         
    }

module.exports={personListController,personCheckIdControler,personDeleteIdControler,personCreateController,personUpdateController}
